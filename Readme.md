# Ansible Kubernetes Raspberry Pi

## Description

I created this Ansible project to set up an K8s cluster with multiple nodes. I helps to set up a K8s Cluster straight forward and without any problems and manual configuration. As CNI it uses Flannel.

## Set Up

All config vars are in the `group_vars` folder. They must be changed for using this script right.

### If you're using a private Container Registry
You also have to replace `/roles/initK8s/vars/gitlab.yml` with your own secret.


## How to use

Run `ansible-playbook -i production/ site.yml` in the console to run the script
